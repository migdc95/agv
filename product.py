class Product:
    type = 0
    components = []  # size 6
    id = 0
    qualityAssessment = True
    remainingTime = 15

    # The class "constructor" - It's actually an initializer
    def __init__(self, _type, _id):
        self.type = _type
        self.assignComponents()
        self.id = _id

    def assignComponents(self):
        if self.type == 1:
            self.components = [1, 0, 1, 2, 0, 0]
        elif self.type == 2:
            self.components = [1, 1, 0, 0, 1, 1]
        elif self.type == 3:
            self.components = [0, 0, 2, 0, 1, 0]
        elif self.type == 4:
            self.components = [0, 1, 1, 1, 0, 0]

    def printComponents(self):

        print("\n")
        print("My components are:\n #1 | #2 | #3 | #4 | #5 | #6 |\n------------------------------")
        for i in self.components:
            print "  " + str(self.components[i]) + " |",

    def qualityControlTest(self, _passed):
        if not _passed:
            self.qualityAssessment = False
            print "The product with id: %s has failed the test." % self.id

    def checkPossibilityOfManufacturing(self, _workStation):
        result = list(map(int.__sub__, _workStation.stationComponents, self.components))

        if self.remainingTime < 14: return True

        if -1 in result or -2 in result:
            return False
        else:
            return True