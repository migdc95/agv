from operator import add
from product import Product
import math

class Location:
    tag = ""
    coordinates = [0, 0]
    radius = 0
    connections = {}
    distance = float("inf")
    visited = False

    def __init__(self, _tag, _x, _y):
        self.tag = _tag
        self.coordinates = [_x, _y]
        self.connections = {}

    def connectLocation(self, _location):
        distance = self.returnDistance(self.coordinates, _location.coordinates)
        name = _location.tag
        if not name in self.connections:
            self.connections[name] = distance
            _location.connections[self.tag] = distance

    def returnDistance(self, a, b):
        return math.sqrt((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2)

    def resetSearch(self):
        self.visited = False
        self.distance = float("inf")


class Station(Location):
    stationComponents = []
    maxCapacity = 3
    id = 0

    def __init__(self,  _tag, _x, _y, _id):
        Location.__init__(self, _tag, _x, _y)
        self.stationComponents = [20, 20, 20, 20, 20, 20]
        self.id = _id


    def addComponents(self, _AGV):
        #print str(self.stationComponents) + " +"
        #print str(_AGV.load)
        self.stationComponents = list(map(add, self.stationComponents, _AGV.load))
        print "\n"
        print "After adding " + str(self.stationComponents)
        print "\n"

    def deleteUsedComponents(self, _product):
        #print str(self.stationComponents) + " -"
        #print str(_product.components)
        self.stationComponents = list(map(int.__sub__, self.stationComponents, _product.components))

        #print "\n"
        #print "After product created " + str(self.stationComponents)
        #print "\n"

    def checkCapacity(self, _component, _amount):
        return self.maxCapacity >= self.stationComponents[_component-1] + _amount

    def failNextProduct(self, _product):
        _product.qualityControlTest(False)




