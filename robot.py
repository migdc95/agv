import numpy as np

class Robot:
    id = 0
    coordinates = [0, 0]
    load = [0, 0, 0, 0, 0, 0]
    orientation = 0
    velocity = [0, 0]
    acceleration = [0, 0]
    tag = ""


    def __init__(self, _tag):
        self.tag = _tag
        pass

    def loadComponent(self, _component):
        index = _component -1
        self.load[index] += 1


    def unloadComponents(self): #When we unload all the load at the assembly station
        self.load = [0, 0, 0, 0, 0, 0]

    def unloadOneComponent(self, _component): #In the case of QC failed and we need to return the component to its origin
        index = _component - 1
        self.load[index] -= 1

    def moveTo(self, _endPoint): #Move the robot to the position passed in the parameters
        pass    #TO BE WRITTEN

    def stop(self): #stop the robot
        pass    #TO BE WRITTEN

    def spin(self, _degrees): #Spin the robot X degrees
        pass    #TO BE WRITTEN

    def planTrajectory(self, _endPoint): #Plan the trajectory from current position to desired destination
        pass    #TO BE WRITTEN

    def updateCoordinates(self, _position): # Update current coordinates
        self.coordinates = _position
        pass  # TO BE WRITTEN

    def getCoordinates(self): # Return current coordinates
        return self.coordinates

    def updateOrientation(self, _orientation): # Update current orientation
        self.orientation = _orientation
        pass  # TO BE WRITTEN

    def getOrientation(self):  # Return current orientation
        return self.orientation

    def distanceToLine(self, p1, p2):
        beginningOfLine = np.asarray(p2)
        endingOfLine = np.asarray(p1)
        robotPosition = np.array(self.coordinates)
        return np.cross(endingOfLine - beginningOfLine, robotPosition - beginningOfLine) / np.linalg.norm(endingOfLine - beginningOfLine)



