from __future__ import generators
import numpy as np
import csv
import random
import sys
import math
import time
import copy
import heapq
import main


def func():
    # Interpret the CSV file
    with open('input_file1.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        print(readCSV)
        input_file = []
        for row in readCSV:
            input_file.append(row[1])

    # Remove first element as this is text
    del input_file[0]
    productSequence = list(map(int, input_file))
    print "product sequence:", productSequence
    return productSequence

def listofcomp(productSequence):
    totalComponents = [0, 0, 0, 0, 0, 0]
    prod_1 = [1, 0, 1, 2, 0, 0]
    prod_2 = [1, 1, 0, 0, 1, 1]
    prod_3 = [0, 0, 2, 0, 1, 0]
    prod_4 = [0, 1, 1, 1, 0, 0]

    for i in range(len(productSequence)):
        if productSequence[i] == 1:
            totalComponents = np.add(totalComponents, prod_1)

        elif productSequence[i] == 2:
            totalComponents = np.add(totalComponents, prod_2)

        elif productSequence[i] == 3:
            totalComponents = np.add(totalComponents, prod_3)

        elif productSequence[i] == 4:
            totalComponents = np.add(totalComponents, prod_4)

    print(totalComponents)
    return totalComponents

def createKnapsacks(componentsSum):

    

    print "sdfghjsakjnf",agvPick


    print('Summation of all component types after potential adjustment: %i' % componentsSum)
    # We make a matrix containing all the knapsacks
    rows = componentsSum / 2
    # used to convert the float into an integer such that we can make a matrix
    rows = int(rows)

    agvPick = np.zeros((rows, 2))

    shape = np.shape(agvPick)
    print('number of knapsacks = %i' % shape[0])
    print('size of each knapsack = %i' % shape[1])
    return agvPick

def createComponentsSequence(productSequence):
    componentSequence = []

    for i in range(len(productSequence)):
        if productSequence[i] == 1:
            componentSequence.append(1)
            componentSequence.append(3)
            componentSequence.append(4)
            componentSequence.append(4)

        elif productSequence[i] == 2:
            componentSequence.append(1)
            componentSequence.append(2)
            componentSequence.append(5)
            componentSequence.append(6)

        elif productSequence[i] == 3:
            componentSequence.append(3)
            componentSequence.append(3)
            componentSequence.append(5)

        elif productSequence[i] == 4:
            componentSequence.append(2)
            componentSequence.append(3)
            componentSequence.append(4)
    return componentSequence

def createGraph():
    graph = {'A': {'WP_1': 120},
             'WP_1': {'A': 120, 'CL_1': 88, 'CL_2': 65, 'CL_3': 50, 'CL_4': 50, 'CL_5': 65, 'CL_6': 88},
             'CL_1': {'WP_1': 88, 'CL_2': 30},
             'CL_2': {'WP_1': 65, 'CL_1': 30, 'CL_3': 30},
             'CL_3': {'WP_1': 50, 'CL_2': 30, 'CL_4': 30},
             'CL_4': {'WP_1': 50, 'CL_3': 30, 'CL_5': 30},
             'CL_5': {'WP_1': 65, 'CL_4': 30, 'CL_6': 30},
             'CL_6': {'WP_1': 88, 'CL_5': 30}}

    #graph = {'A': {'B': 2, 'C': 3}, 'B': {'A': 3, 'D': 5}, 'C':{'A':3, 'D': 5}, 'D':{'C': 5, 'B': 5}}
    return graph

class priorityDictionary(dict):
    def __init__(self):
        self.__heap = []
        dict.__init__(self)

    def smallest(self):
        if len(self) == 0:
            raise IndexError
        heap = self.__heap
        while heap[0][1] not in self or self[heap[0][1]] != heap[0][0]:
            lastItem = heap.pop()
            insertionPoint = 0
            while 1:
                smallChild = 2 * insertionPoint + 1
                if smallChild + 1 < len(heap) and heap[smallChild] > heap[smallChild + 1]:
                    smallChild += 1
                if smallChild >= len(heap) or lastItem <= heap[smallChild]:
                    heap[insertionPoint] = lastItem
                    break
                heap[insertionPoint] = heap[smallChild]
                insertionPoint = smallChild
        return heap[0][1]

    def __iter__(self):

        def iterfn():
            while len(self) > 0:
                x = self.smallest()
                yield x
                del self[x]

        return iterfn()

    def __setitem__(self, key, val):
        dict.__setitem__(self, key, val)
        heap = self.__heap
        if len(heap) > 2 * len(self):
            self.__heap = [(v, k) for k, v in self.iteritems()]
            self.__heap.sort()  # builtin sort probably faster than O(n)-time heapify
        else:
            newPair = (val, key)
            insertionPoint = len(heap)
            heap.append(None)
            while insertionPoint > 0 and newPair < heap[(insertionPoint - 1) // 2]:
                heap[insertionPoint] = heap[(insertionPoint - 1) // 2]
                insertionPoint = (insertionPoint - 1) // 2
            heap[insertionPoint] = newPair

    def setdefault(self, key, val):
        if key not in self:
            self[key] = val
        return self[key]


def Dijkstra(G, start, end=None):
    D = {}  # dictionary of final distances
    P = {}  # dictionary of predecessors
    Q = priorityDictionary()  # est.dist. of non-final vert.
    pathDist = 0
    Q[start] = 0

    for v in Q:
        D[v] = Q[v]
        if v == end: break

        for w in G[v]:
            vwLength = D[v] + G[v][w]
            if w in D:
                if vwLength < D[w]:
                    raise ValueError
            elif w not in Q or vwLength < Q[w]:
                Q[w] = vwLength
                P[w] = v

    return (D, P, pathDist)


def shortestPath(G, start, end):
    D, P, pathDist = Dijkstra(G, start, end)
    Path = []
    while 1:
        Path.append(end)
        if end == start: break
        end = P[end]
    Path.reverse()
    return (Path, D)

# ---------------------------------------------------------------
# -------------------- TOTAL TRAVEL DISTANCE -------------------
# ---------------------------------------------------------------

def totalTravelDistance(knapsack, graph):

    total_distance = 0
    route_distance = 0

    for i in range(len(knapsack)):
        # Distance from assembly to first component location
        firstDist = shortestPath(graph, 'workStation', knapsack[i][0].tag)
        firstDist = firstDist[1][knapsack[i][0].tag]

        # Distance from first component location to second component location
        secondDist = shortestPath(graph, knapsack[i][0].tag, knapsack[i][1].tag)
        secondDist = secondDist[1][knapsack[i][1].tag]

        # Distance from second component location to assembly
        thirdDist = shortestPath(graph, knapsack[i][1].tag, 'workStation')
        thirdDist = thirdDist[1]['workStation']
        # print(thirdDist[0])
        # print(thirdDist[1]['A'])

        route_distance = firstDist + secondDist + thirdDist

        total_distance += route_distance

    return total_distance


def neighbourFunctionSA(knapsack, graph):
    neighbourKnapsack = copy.deepcopy(knapsack)

    for i in range(1):
        ran_i = random.randint(0, len(knapsack) - 1)
        ran_k = random.randint(0, len(knapsack[0]) - 1)
        #print('i is equal to %i and j is %i' % (ran_i, ran_k))

        ran_j = random.randint(0, len(knapsack) - 1)
        ran_n = random.randint(0, len(knapsack[0]) - 1)

        neighbourKnapsack[ran_i][ran_k] = knapsack[ran_j] [ran_n]
        neighbourKnapsack[ran_j][ran_n] = knapsack[ran_i] [ran_k]

        #print('Changing item (%i, %i) = %i with item (%i, %i) = %i' % (ran_i, ran_k, knapsack[ran_i, ran_k], ran_j, ran_n, knapsack[ran_j, ran_n]))

    weight = totalTravelDistance(neighbourKnapsack, graph)

    neighbourSolution = [weight, neighbourKnapsack]

    return neighbourSolution

def neighbourFunctionHC(knapsack, graph):
    bestKnapsack = copy.deepcopy(knapsack)
    neighbourKnapsack = copy.deepcopy(knapsack)

    for i in range(len(knapsack)):
        #print('CHANGE THE VARIABLE I -------------- i is equal to %i' % i)
        for j in range(len(knapsack[0])):
            #print('CHANGE THE VARIABLE J (+1)')
            for k in range(len(knapsack)):
                if i !=k:
                    #print('CHANGE THE VARIABLE K -------------- k is equal to %i' % k)
                    for n in range(len(knapsack[0])):
                        #print('CHANGE THE VARIABLE N (+1)') 
                        neighbourKnapsack[i][j] = knapsack[k][n]
                        neighbourKnapsack[k][n] = knapsack[i][j]

                        dist1 = totalTravelDistance(neighbourKnapsack, graph)
                        dist2 = totalTravelDistance(bestKnapsack, graph)

                        if dist1 < dist2:
                            bestKnapsack = copy.deepcopy(neighbourKnapsack)
                            neighbourKnapsack = copy.deepcopy(knapsack)
                                #h += 1
                                #print(h)
                        else:
                            neighbourKnapsack = copy.deepcopy(knapsack)

    bestNeigh = [totalTravelDistance(bestKnapsack, graph), bestKnapsack]

    return bestNeigh


def simulatedAnnealing(initialKnapsack, graph, T_min, T_max, decrease):
    # Graph stuff
    bestPerIteration = []
    iterationNumber = []
    count = 0
    failure = 0
    succes = 0

    bestKnapsack = [totalTravelDistance(initialKnapsack, graph), initialKnapsack]
    overallBest = [sys.maxsize, 0]

    #for timing purposes (not needed for the code to work)

    while (T_max > T_min):
        count += 1
        currentKnapsack = neighbourFunctionSA(bestKnapsack[1], graph)

        # If the neighbourhood solution has a better (i.e. lower) value than the current solution
        if currentKnapsack[0] < bestKnapsack[0]:
            bestKnapsack = copy.deepcopy(currentKnapsack)

            if bestKnapsack[0] < overallBest[0]:
                overallBest = copy.deepcopy(bestKnapsack)

            # For the purpose of creating graphs/illustrations
            bestPerIteration.append(bestKnapsack[0])
            iterationNumber.append(count)

        else:
            try:
                chance = math.exp((bestKnapsack[0]-currentKnapsack[0]) / T_max)
                failure += 1
            except OverflowError:
                chance = float('inf')

            if chance > random.uniform(0, 1):
               # print("*******************************************Worse Solution Accepted*******************************")

                bestKnapsack = copy.deepcopy(currentKnapsack)

                # For the purpose of creating graphs/illustrations
                bestPerIteration.append(bestKnapsack[0])
                iterationNumber.append(count)
                succes += 1


        T_max = T_max*decrease

    return overallBest


def batching(productSequence, componentsBatchSize):
    batch = []

    while (len(productSequence) >= componentsBatchSize):
        batch.append(productSequence[0:componentsBatchSize])
        del productSequence[0:componentsBatchSize]

    if len(productSequence) > 0:
        batch.append(productSequence)

    return batch


def hillClimbing(initialKnapsack, graph):
    start = time.time()
    elapsed = 0
    bestSolution = [totalTravelDistance(initialKnapsack, graph), initialKnapsack]

    while (elapsed < 32):
        print("FINDING NEW NEIGHBOUR---------------------------")
        bestNeigh = neighbourFunctionHC(bestSolution[1], graph)

        # If bestNeigh[2] = 1, there have been found a better neighbour solution, else bestNeigh[2] = 0
       # if (bestNeigh[0] = bestSolution[0]):
        if (bestNeigh[0] < bestSolution[0]):
            #print(bestNeigh)
            bestSolution = copy.deepcopy(bestNeigh)

        else:
            print('we stop early')
            break

        elapsed = time.time() - start

    return bestSolution






