import rospy
import tf
from geometry_msgs.msg import Twist, Point, TransformStamped
from math import atan2, sqrt, pow
from std_msgs.msg import String, Float64, Int32
from hello_ros.msg import agvMessages
import csv
import math
from product import Product
from location import Location
from location import Station
from robot import Robot
from operator import attrgetter
from priodict import priorityDictionary
import time
import random
import pprint
from multiprocessing import Pool
from numpy import ones, vstack
from numpy.linalg import lstsq
import ImportCSV
import time
import copy
import threading

# ---------------------------------------------------------------
# --------------- GLOBAL VARIABLES DECLARATION ------------------
# ---------------------------------------------------------------

listOfProducts = []
componentsSequence = []
passPoints = []
componentLocations = []
allLocations = []
failNextOne = False
distanceToWalls = 10  # Distance to the walls of the passing points in cm
agv = Robot("robot1")
workStation = Station("workStation", 0, 30, 1)
transformationMatrix = [126, 34]
rotationAroundXAxis = 0
distanceToObjectiveThreshold = 6  # Distance in cm
schedulingMode = 1 # 1 for FCFS; 2 for HC; 3 for SA; 4 for ASA

graph = {}
global workstation
global knapsack
knapsack = []
global remainingComponents
remainingComponents = [0,0,0,0,0,0]




# ---------------------------------------------------------------
# -------------- ROS PUBLISHER AND SUBSCRIBER -------------------
# ---------------------------------------------------------------

pubCustom = rospy.Publisher('/AGV4', agvMessages, queue_size=10)
customMessage = agvMessages()
rob = TransformStamped()

# ---------------------------------------------------------------
# ---------------------- VICON CALL BACK ------------------------
# ---------------------------------------------------------------

def viconCallBack(data):
	global rob
	global rotationAroundXAxis
	rob = data  # callback function for ROS subscriber
	agv.updateCoordinates([int(rob.transform.translation.x * 100) - transformationMatrix[0],
						   int(rob.transform.translation.y * 100) - transformationMatrix[1]])
	rot_rob = rob.transform.rotation  # assign orientation
	(r, p, rotationAroundXAxis) = tf.transformations.euler_from_quaternion([rot_rob.x, rot_rob.y, rot_rob.z, rot_rob.w])
	agv.updateOrientation(rotationAroundXAxis)



def assembleProduct():
	global qualityControl, productAtQuality, assemblying
	qualityControl = True
	assemblying = True
	startingTime = productAtAssemblying.remainingTime

	for i in range(0, startingTime):
		if qualityControl:
			time.sleep(1)
			productAtAssemblying.remainingTime -= 1
			print "%s seconds remaining of product %s" %(productAtAssemblying.remainingTime, productAtAssemblying.type)
		else:
			productAtAssemblying.remainingTime -= i
			assemblying = False
			print "Putting in stand by product ", productAtAssemblying.type, assemblying
			return			

	assemblying = False
	print "------------------------------------"
	print "\n", "\n"
	print "Finished product", productAtAssemblying.type
	print "\n", "\n"
	print "------------------------------------"
	print "\n", "\n"

	productAtQuality = productAtAssemblying
	qualityThread = threading.Thread(target=qualityControlFunction)
	qualityThread.start()
	if len(listOfProducts) > 0:
		startAssemblyingNextProduct()

	
def qualityControlFunction():
	global qualityControl, productAtQuality
	time.sleep(5)
	print "Testing product %s" % (productAtQuality.type)
	if random.uniform(0, 1) >= 0.75: # 75% RATE OF SUCCESS
		productAtQuality.remainingTime = 15
		qualityControl = False
		listOfProducts.insert(0, productAtAssemblying)
		listOfProducts.insert(0, productAtQuality)
		print "------------------------------------"
		print "\n", "\n"
		print "Product", productAtQuality.type, "FAILED quality control"
		print "\n", "\n"
		print "------------------------------------"
		time.sleep(1)
		startAssemblyingNextProduct()
		return

	print "------------------------------------"
	print "\n", "\n"
	print "Product", productAtQuality.type, "passed quality control"
	print "\n", "\n"
	print "------------------------------------"
	

# ---------------------------------------------------------------
# ---------------------------- MAIN -----------------------------
# ---------------------------------------------------------------

def main():
	global initialTime, endTime, arrayOfValues, arrayOfCoordinates, nextComponents
	ended = False
	global assemblying
	arrayOfValues = []
	arrayOfCoordinates = []
	
	endTime = 0
	arrayOfValues = []
	arrayOfCoordinates = []
	setUp()
	
	global productAtAssemblying
	productAtAssemblying = listOfProducts[0]
	global productAtQuality
	productAtQuality = listOfProducts[0]

	# create subscriber to the topic of our robot in Vicon
	kpos = rospy.Subscriber('/vicon/AGVgroup4/AGVgroup4', TransformStamped, viconCallBack, queue_size=80)
	agvPosition = Point()  # define variable to store vicon data
	time.sleep(2)

	global previousPosition
	previousPosition = agv.coordinates
	global totalEstimatedDistance
	totalEstimatedDistance = 0
	global totalDistance
	totalDistance = 0

	initialTime = time.time()
	if schedulingMode == 1: nextComponents = firstComeFirstServe()
	if schedulingMode == 2: nextComponents = setUpAssemblyStorage()
	if schedulingMode == 3: nextComponents = setUpForHillClimbing()
	if schedulingMode == 4: nextComponents = setUpSA()

	print time.time() - initialTime, "for finding best knapsack"
	print "length of the product list:", len(listOfProducts)
	print "length of the nextComponents list:", len(nextComponents)

	#startAssemblyingNextProduct() # JUST HERE FOR TESTING P.A AND Q.C.
	
	while not rospy.is_shutdown():
		
		while len(listOfProducts) > 0:

			while len(nextComponents) > 0:

				# CHECK IF WE CAN STORE THE NEXT COMPONENTS IN THE ASSEMBLY CAPACITY
				if returnObjectFromTag("workstation").checkCapacity(nextComponents[0].type, 1) and returnObjectFromTag("workstation").checkCapacity(nextComponents[1].type, 1):
					pass
				else:
					nextComponents = setUpAssemblyStorage() #THIS IS BECAUSE THIS APPROACH TAKES THE POSSIBLE COMPONENTS


				nextObjective = nextComponents[0]  # First position of the components list i.e. [comp.1, comp.2]. They are the object, not just the tag
				trip = findPathTo(agv, nextObjective[0])
				doTrip(trip)


				stringOfElement = nextObjective[0].tag
				numberOfElement = stringOfElement[len(stringOfElement) - 1]

				if nextObjective[0].tag == nextObjective[1].tag:  # We have to pick up two components from same type
					agv.loadComponent(int(numberOfElement))
					agv.loadComponent(int(numberOfElement))
					time.sleep(1)
					goBackToAssemblyStation()
					workStation.addComponents(agv)
					agv.unloadComponents()
				else:  # We have to pick up two different components
					agv.loadComponent(int(numberOfElement))
					trip = findPathTo(agv, nextObjective[1])
					doTrip(trip)
					stringOfElement = nextObjective[1].tag
					numberOfElement = stringOfElement[len(stringOfElement) - 1]
					agv.loadComponent(int(numberOfElement))
					goBackToAssemblyStation()
					workStation.addComponents(agv)
					agv.unloadComponents()

				nextComponents.pop(0)
				print "length of the product list:", len(listOfProducts)				

				# Now we should check whether the next product in the list can be assembled
				if len(listOfProducts) > 0:
					startAssemblyingNextProduct()
					
				ra.sleep()



		if not ended:
			ended = True
			endTime = time.time()
			print "Total distance covered: %s. Ideal distance covered: %s." %(totalDistance, totalEstimatedDistance)
			print "This is a XX percent more than the ideal scenario",(100*totalDistance/totalEstimatedDistance)
			print "-------------------------------"
			print "\n","\n"
			print "Time(s):", endTime - initialTime
			print "\n","\n"
			print "-------------------------------"

			writeCSV() # HERE WE CALL ONCE WE FINISH THE FUNCTION TO STORE THE VALUES IN .CSV



# ---------------------------------------------------------------
# -------------------------- SET UP -----------------------------
# ---------------------------------------------------------------

def setUp():
	rospy.init_node('hoooo')
	global ra, assemblying, qualityControl
	ra = rospy.Rate(10)
	print "READY"
	assemblying = False
	qualityControl = True

	read_file()

	# Create passPoints
	passPoints.append(Location("ppA", -90 - distanceToWalls, 130 - distanceToWalls))
	passPoints.append(Location("ppB", -90 - distanceToWalls, 135 + distanceToWalls))
	passPoints.append(Location("ppC", 0, 130 - distanceToWalls))  # middle
	passPoints.append(Location("ppD1", 7, 145 + distanceToWalls))  # This is one of the points that substitutes ppD. Needs to be adjusted so that it does not crash into the walls
	passPoints.append(Location("ppE", +90 + distanceToWalls, 130 - distanceToWalls))
	passPoints.append(Location("ppF", +90 + distanceToWalls, 135 + distanceToWalls))
	passPoints.append(Location("ppD2", -7, 145 + distanceToWalls))  # This is one of the points that substitutes ppD. Needs to be adjusted so that it does not crash into the walls

	# Create component stations
	componentLocations.append(Location("component1", 75, 175))
	componentLocations.append(Location("component2", 45, 175))
	componentLocations.append(Location("component3", 15, 175))
	componentLocations.append(Location("component4", -15, 175))
	componentLocations.append(Location("component5", -45, 175))
	componentLocations.append(Location("component6", -75, 175))

	# Connect component locations with B, D and F
	for comp in componentLocations:
		comp.connectLocation(passPoints[6])
		for pPoints in passPoints[1::2]:
			comp.connectLocation(pPoints)

	for c in xrange(0, len(componentLocations) - 2):
		componentLocations[c].connectLocation(componentLocations[c + 1])

	# Connect pass points between them and A, C and E with the workstation
	for i in xrange(0, len(passPoints) - 1, 2):
		passPoints[i].connectLocation(passPoints[i + 1])
		passPoints[i].connectLocation(workStation)
	passPoints[2].connectLocation(passPoints[6])

	allLocations.append(workStation)

	for comp in componentLocations:
		allLocations.append(comp)

	for pPoints in passPoints:
		allLocations.append(pPoints)


	# Add all the locations to the Graph except the
	for location in allLocations:
		graph[location.tag] = location.connections
		if not location.tag in graph:
			graph[location.tag] = location.connections

	
	allLocations.append(agv)
	





# ---------------------------------------------------------------
# ---------- MOVE THE ROBOT TO THE ASSEMBLY STATION -------------
# ---------------------------------------------------------------

def goBackToAssemblyStation():
	trip = findPathTo(agv, workStation)
	trip.pop(2)
	doTrip(trip)
	time.sleep(1) # We add an extra second to unload the second component



# ---------------------------------------------------------------
# ------------------ DISTANCE BETWEEN 2 POINTS ------------------
# ---------------------------------------------------------------

def doTrip(trip):
	global destination, totalEstimatedDistance
	
	for i in xrange(0, len(trip) - 1):

		origin = returnObjectFromTag(trip[i])
		destination = returnObjectFromTag(trip[i + 1])

		totalEstimatedDistance += returnDistance(origin.coordinates, destination.coordinates)

		ratio = 255.0 / 360.0

		if i == 0: 
			angleToRotate = rotateToObjective(destination.coordinates, agv.coordinates, agv.orientation)  # rotate towards first objective
			if angleToRotate >= 90 and angleToRotate <= 180:
				angleToRotate += 180
			elif angleToRotate > 180 and angleToRotate <= 270:
				angleToRotate -= 180
			# print "angleToRotate:", angleToRotate
			angleToRotate = angleToRotate * ratio - 128.0

			if angleToRotate > -124.45 and angleToRotate < 123.46:
				spinRobot(angleToRotate)

		distanceToObjective = returnDistance(destination.coordinates, agv.coordinates)

		if distanceToObjective > distanceToObjectiveThreshold:
			pidControl(distanceToObjective) # COMMENT THIS FOR THE ARDUINO TEST

	customMessage.mode = 3  # Stop the robot after reaching the last position
	pubCustom.publish(customMessage)
	ra.sleep()
	time.sleep(1) # Wait 1 second to load/unload one component



# ---------------------------------------------------------------
# ------------------------- PID CONTROL  ------------------------
# ---------------------------------------------------------------

def pidControl(_distanceToObjective):
	kp = 0.6
	kd = 3
	ki = 0
	desiredSpeed = 100
	rightSpeed = desiredSpeed
	leftSpeed = desiredSpeed
	error = 0
	previousError = 0

	distanceToObjective = _distanceToObjective
	cumulativeError = 0

	speedKp = 0.8

	global totalDistance
	global previousPosition

	while distanceToObjective > distanceToObjectiveThreshold:
		error = rotateToObjective(destination.coordinates, agv.coordinates, agv.orientation)
		totalDistance += returnDistance(agv.coordinates, previousPosition)
		previousPosition = agv.coordinates

		corrected = True

		if error >= 5 and error < 90:
			correction = int(error * kp + (error - previousError) * kd + cumulativeError * ki)

			if rightSpeed + correction <= 127 and rightSpeed + correction >= -128:
				rightSpeed += correction

			if leftSpeed - correction <= 127 and leftSpeed - correction >= -128:
				leftSpeed -= correction
			customMessage.mode = 1

		elif error >= 90 and error <= 175:
			error = 180 - error
			correction = int(error * kp + (error - previousError) * kd + cumulativeError * ki)

			if rightSpeed + correction <= 127 and rightSpeed + correction >= -128:
				rightSpeed += correction

			if leftSpeed - correction <= 127 and leftSpeed - correction >= -128:
				leftSpeed -= correction

			customMessage.mode = 2

		elif error >= 185 and error < 270:
			error = error - 180
			correction = int(error * kp + (error - previousError) * kd + cumulativeError * ki)

			if leftSpeed + correction <= 127 and leftSpeed + correction >= -128:
				leftSpeed += correction

			if rightSpeed - correction <= 127 and rightSpeed - correction >= -128:
				rightSpeed -= correction

			customMessage.mode = 2

		elif error >= 270 and error <= 355:
			error = 360 - error
			correction = int(error * kp + (error - previousError) * kd + cumulativeError * ki)

			if leftSpeed + correction <= 127 and leftSpeed + correction >= -128:
				leftSpeed += correction

			if rightSpeed - correction >= -128 and rightSpeed - correction <= 127:
				rightSpeed -= correction

			customMessage.mode = 1

		else:
			corrected = False
			if error < 5 or error > 355:
				customMessage.mode = 1
			elif error > 175 and error < 185:
				customMessage.mode = 2

			if distanceToObjective > 15:
				maximumSpeed = 210
				reduction = int(speedKp / distanceToObjective)
				leftSpeed = maximumSpeed - 128 - reduction
				rightSpeed = maximumSpeed - 128 - reduction

		if corrected:
			conversion = convertToMaxSpeed(leftSpeed, rightSpeed, 200.0)
			leftSpeed = conversion[0]
			rightSpeed = conversion[1]

		arrayOfCoordinates.append([agv.coordinates[0], agv.coordinates[1], time.time() - initialTime]) # HERE WE STORE THE VALUES OF THE COORDINATES

		previousError = error
		distanceToObjective = int(returnDistance(destination.coordinates, agv.coordinates))

		customMessage.leftVelocity = leftSpeed
		customMessage.rightVelocity = rightSpeed
		pubCustom.publish(customMessage)

		ra.sleep()



# ---------------------------------------------------------------
# ----------------- CONVERT PD OUTPUT TO MAX SPEED  -------------
# ---------------------------------------------------------------
def convertToMaxSpeed(_left, _right, _max):
	tempLeft = _left + 128
	tempRight = _right + 128

	ratio = 1.0
	maxSpeed = _max

	if tempRight > tempLeft: 
		ratio = (maxSpeed)/float(tempRight)
	else: 
		ratio = (maxSpeed)/float(tempLeft)

	temporalRightSpeed = tempRight * ratio
	temporalLeftSpeed = tempLeft * ratio

	if temporalLeftSpeed <= 254:
		leftSpeed = int(temporalLeftSpeed)
	else:
		print int(temporalLeftSpeed), int(temporalRightSpeed)

	if temporalRightSpeed <= 254:
		rightSpeed = int(temporalRightSpeed)
	else:
		print int(temporalLeftSpeed), int(temporalRightSpeed)

	return [leftSpeed - 128, rightSpeed - 128]



# ---------------------------------------------------------------
# ------------------ DISTANCE BETWEEN 2 POINTS ------------------
# ---------------------------------------------------------------

def returnDistance(a, b):
	return math.sqrt((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2)


# ---------------------------------------------------------------
# ------------------ GET OBJECT FROM TAG NAME -------------------
# ---------------------------------------------------------------

def returnObjectFromTag(tag):  # Returns the object associated with the tag. The tag is the only information we have from the Dijkstra return.
	# We need the object as it contains all the information as position, type of component etc.
	for each in allLocations:
		if each.tag == tag:
			return each
	return "error"

# ---------------------------------------------------------------
# -------------- GET SHORTEST NODE PATH FROM A TO B -------------
# ---------------------------------------------------------------

def findPathTo(robot, _end):
	agvDictionary = {}
	endTag = _end.tag
	origin = robot.coordinates
	tag = robot.tag
	if tag == "robot1":
		if origin[1] > 117:  # Above the walls. Connect with PPB, PPD, PPF and Components Locations
			for pPoints in passPoints[1::2]:
				agvDictionary[pPoints.tag] = returnDistance(origin, pPoints.coordinates)
			agvDictionary[passPoints[6].tag] = returnDistance(origin, passPoints[6].coordinates)

			for comp in componentLocations:
				agvDictionary[comp.tag] = returnDistance(origin, comp.coordinates)

		else:  # Below the walls. Connect with PPA, PPC, PPE and Workstation
			for pPoints in passPoints[0::2]:
				if pPoints.tag == "ppD2":
					pass
				else:
					agvDictionary[pPoints.tag] = returnDistance(origin, pPoints.coordinates)

			agvDictionary[workStation.tag] = returnDistance(origin, workStation.coordinates)

		graph[tag] = agvDictionary

	return shortestPath(graph, tag, endTag)[0]


def Dijkstra(G, start, end=None):
	D = {}  # dictionary of final distances
	P = {}  # dictionary of predecessors
	Q = priorityDictionary()  # est.dist. of non-final vert.
	Q[start] = 0

	for v in Q:
		D[v] = Q[v]
		if v == end: break

		for w in G[v]:
			vwLength = D[v] + G[v][w]
			if w in D:
				if vwLength < D[w]:
					raise ValueError, \
						"Dijkstra: found better path to already-final vertex"
			elif w not in Q or vwLength < Q[w]:
				Q[w] = vwLength
				P[w] = v
	return D, P


def shortestPath(G, start, end):
	D, P = Dijkstra(G, start, end)
	Path = []
	while 1:
		Path.append(end)
		if end == start: break
		end = P[end]
	Path.reverse()
	return Path, D

# ---------------------------------------------------------------
# ----------- GET ANGLE NEEDED TO ROTATE TO OBJECTIVE -----------
# ---------------------------------------------------------------

def rotateToObjective(_objetive, _starting, _orientation):  # Objective and Starting in [x,y]; Orientation in radians

	x2 = _objetive[0]
	y2 = _objetive[1]

	x1 = _starting[0]
	y1 = _starting[1]

	coordinatesAngle = math.degrees(math.atan2(x2 - x1, y2 - y1))

	desiredRotation = (coordinatesAngle + math.degrees(_orientation)) * -1

	if desiredRotation < 0: desiredRotation += 360

	desiredRotation = int(desiredRotation)

	return desiredRotation


# ---------------------------------------------------------------
# ----------- SEND MESSAGE TO ROTATE THE ROBOT -----------
# ---------------------------------------------------------------

def spinRobot(_angle):  # Objective and Starting in [x,y]; Orientation in radians
	customMessage.mode = 0
	customMessage.rotationSpeed = 0
	customMessage.rotate = int(_angle)
	pubCustom.publish(customMessage)
	ra.sleep()


# ---------------------------------------------------------------
# ----------- READ CSV FILE AND CREATE LIST OF PRODUCTS ---------
# ---------------------------------------------------------------

def read_file():
	file = open("only5products.csv", "r")
	ids = list(csv.reader(file, delimiter=","))

	count = 0
	for i in ids:
		productType = int(
			i[1][1])  # Get the second column (Px) and get the second element of the String, which is the number
		listOfProducts.append(Product(productType, count))
		evaluatedElement = listOfProducts[len(listOfProducts) - 1]
		for cp in range(6):
			if evaluatedElement.components[cp] == 1:
				componentsSequence.append(cp+1)
				remainingComponents[cp] += 1
			if evaluatedElement.components[cp] == 2:
				componentsSequence.append(cp+1)
				componentsSequence.append(cp+1)
				remainingComponents[cp] += 2
		count += 1


# ---------------------------------------------------------------
# -------- SAVE VALUES OF DIFFERENT PARAMETERS TO A CSV ---------
# ---------------------------------------------------------------

def writeCSV():
	'''
	# ----------- FROM HERE ---------------------#
	kp = 0.8
	kd = 3.3
	ki = 0
	version = "3"
	filename = "/home/migdc95/PycharmProjects/plotter/PID-kp-" + str(kp) + "-kd-" + str(kd) + "-ki-" + str(ki) + "-v" + version + ".csv"
	with open(filename, 'w') as csvfile:
		filewriter = csv.writer(csvfile, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)
		for each in arrayOfValues:
			filewriter.writerow(each)

	# ----------- UNTIL HERE -------------------#
	# CAN BE COMMENTED AS IT WAS FOR THE PID
	'''
	# ------------- FROM HERE ------------------#

	print "Saving file of coordinates"

	with open('/home/migdc95/PycharmProjects/plotter/coordinates-totalDistance10.csv', 'w') as csvfile: # CHOOSE YOUR PREFFERED DIRECTORY
		filewriter = csv.writer(csvfile, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)
		for each in arrayOfCoordinates:
			filewriter.writerow(each)

	# ------------ UNTIL HERE ------------------#
	# IS FOR THE COORDINATES. IF YOU USE IT, THEN REMEMBER TO CHANGE THE NAME OF THE FILE EVERYTIME. OTHERWISE IT WILL OVERWRITTE


# ----------------------------------------------------------------
# - CHECK IF IT IS POSSIBLE TO ASSEMBLE NEXT PRODUCT ON THE LIST -
# ----------------------------------------------------------------
def startAssemblyingNextProduct():
	global productAtAssemblying, endTime, nextComponents, assemblying

	if listOfProducts[0].checkPossibilityOfManufacturing(returnObjectFromTag("workStation")) and not assemblying:
		endTime = time.time()
		print "--------------------------------------------------------"
		print "--------------------------------------------------------"
		print "Started assemblying product", listOfProducts[0].type, "..."
		print "Time(s):", endTime - initialTime
		print "--------------------------------------------------------"
		print "--------------------------------------------------------"
		
		if listOfProducts[0].remainingTime == 15:
			returnObjectFromTag("workStation").deleteUsedComponents(listOfProducts[0])
		else: 
			print "This prodcut had already been started to be assembled, so we do not take more components from the storage"

		print "--------------------------------------------------------"
		productAtAssemblying = listOfProducts[0]
		threads = threading.Thread(target=assembleProduct)
		threads.start()
		# ONLY FOR FIRST COME FIRST SERVE
		listOfProducts.pop(0)
		if len(listOfProducts) > 0:
			if schedulingMode == 4: nextComponents = setUpAssemblyStorage()
			if schedulingMode == 1: nextComponents = firstComeFirstServe()
	else:
		print "We don't have enough components to assemble product", listOfProducts[0].type

	
	

# ---------------------------------------------------------------
# -------------------- TOTAL TRAVEL DISTANCE -------------------
# ---------------------------------------------------------------
def totalTravelDistance(knapsack, graph):

	total_distance = 0
	route_distance = 0

	for i in range(len(knapsack)):
		# Distance from assembly to first component location
		firstDist = shortestPath(graph, 'workStation', knapsack[i][0].tag)
		firstDist = firstDist[1][knapsack[i][0].tag]

		# Distance from first component location to second component location
		secondDist = shortestPath(graph, knapsack[i][0].tag, knapsack[i][1].tag)
		secondDist = secondDist[1][knapsack[i][1].tag]

		# Distance from second component location to assembly
		thirdDist = shortestPath(graph, knapsack[i][1].tag, 'workStation')
		thirdDist = thirdDist[1]['workStation']

		route_distance = firstDist + secondDist + thirdDist
		total_distance += route_distance

	return total_distance



#----------------------------------------------------#
#------------------- HILL CLIMBING ------------------#
#----------------------------------------------------#
def setUpForHillClimbing():

	bathcSize = 18
	# Create the product batch (5 products) - The number will have to be tested.
	componentBatch = componentsSequence[0:bathcSize]

	for i in range(0 ,len(componentBatch), 2):
		comp1 = "component"+ str(componentBatch[i])
		comp2 = "component"+ str(componentBatch[i+1])
		knapsack.append([returnObjectFromTag(comp1), returnObjectFromTag(comp2)])

	weight = totalTravelDistance(knapsack, graph)

	# Neighbourhood function
	k = ImportCSV.hillClimbing(knapsack, graph)[1]

	print "batch:", k

	for each in k:
		for a in each:
			print a.tag

	return k


#----------------------------------------------------#
#---------------- SIMULATED ANNEALING ---------------#
#----------------------------------------------------#
def setUpSA():

	bathcSize = 18
	# Create the product batch (5 products) - The number will have to be tested.
	componentBatch = componentsSequence[0:bathcSize]

	for i in range(0 ,len(componentBatch), 2):
		comp1 = "component"+ str(componentBatch[i])
		comp2 = "component"+ str(componentBatch[i+1])
		knapsack.append([returnObjectFromTag(comp1), returnObjectFromTag(comp2)])

	weight = totalTravelDistance(knapsack, graph)

	start = time.time()

	simA = ImportCSV.simulatedAnnealing(knapsack, graph, 0.5, 30, 0.98)[1]

	for each in simA:
		for a in each:
			print a.tag

	return simA



#----------------------------------------------------#
#---------------- ASSEMBLLY STORAGE APP. ------------#
#----------------------------------------------------#
def setUpAssemblyStorage():

	bathcSize = 18
	# Create the product batch (5 products) - The number will have to be tested.
	componentBatch = componentsSequence[0:bathcSize]

	for i in range(0 ,len(componentBatch), 2):
		comp1 = "component"+ str(componentBatch[i])
		comp2 = "component"+ str(componentBatch[i+1])
		knapsack.append([returnObjectFromTag(comp1), returnObjectFromTag(comp2)])

	weight = totalTravelDistance(knapsack, graph)

	if (returnObjectFromTag("workStation").stationComponents[0] < 2 and remainingComponents[0] > 1):
		print('get two of component 1')
		agvPick = [returnObjectFromTag("component1"), returnObjectFromTag("component1")]

	elif returnObjectFromTag("workStation").stationComponents[1] < 2 and remainingComponents[1] > 1:
		print('get two of component 2')
		agvPick = [returnObjectFromTag("component2"), returnObjectFromTag("component2")]

	elif returnObjectFromTag("workStation").stationComponents[2] < 2 and remainingComponents[2] > 1:
		print('get two of component 3')
		agvPick = [returnObjectFromTag("component3"), returnObjectFromTag("component3")]

	elif returnObjectFromTag("workStation").stationComponents[3] < 2 and remainingComponents[3] > 1:
		print('get two of component 4')
		agvPick = [returnObjectFromTag("component4"), returnObjectFromTag("component4")]

	elif returnObjectFromTag("workStation").stationComponents[4] < 2 and remainingComponents[4] > 1:
		print('get two of component 5')
		agvPick = [returnObjectFromTag("component5"), returnObjectFromTag("component5")]

	elif returnObjectFromTag("workStation").stationComponents[5] < 2 and remainingComponents[5] > 1:
		print('get two of component 6')
		agvPick = [returnObjectFromTag("component6"), returnObjectFromTag("component6")]

	# If we do not need 2 of the same component any longer
	else:
		tot = remainingComponents[0]+remainingComponents[1]+remainingComponents[2]
		if tot >= 2:
			for i in range(len(remainingComponents)):
				if remainingComponents[i] == 1:
					agvPick.append(returnObjectFromTag("component" + str(i+1)))
					if len(agvPick) == 2:
						return agvPick
						#break

		else:
			for i in range(len(remainingComponents)):
				leng = len(remainingComponents)-i
				if remainingComponents[i] == 1:
					agvPick.append(returnObjectFromTag("component" + str(leng)))
					if len(agvPick) == 2:
						return agvPick
						#break
	return [agvPick]




#----------------------------------------------------#
#---------------- FIRST COME FIRST SERVE ------------#
#----------------------------------------------------#
def firstComeFirstServe():

	if listOfProducts[0].type == 1:
		return [[returnObjectFromTag("component1"), returnObjectFromTag("component3")],
		[returnObjectFromTag("component4"), returnObjectFromTag("component4")]]
	elif listOfProducts[0].type == 2:
		return [[returnObjectFromTag("component1"), returnObjectFromTag("component2")],
		[returnObjectFromTag("component5"), returnObjectFromTag("component6")]]
	elif listOfProducts[0].type == 3:
		return [[returnObjectFromTag("component3"), returnObjectFromTag("component3")],
		[returnObjectFromTag("component5"), returnObjectFromTag("component5")]]
	elif listOfProducts[0].type == 4:
		return [[returnObjectFromTag("component2"), returnObjectFromTag("component3")],
		[returnObjectFromTag("component4"), returnObjectFromTag("component4")]]


# ---------------------------------------------------------------
# -------------------- CALL THE MAIN FUNCTION -------------------
# ---------------------------------------------------------------
if __name__ == '__main__':
	main()
